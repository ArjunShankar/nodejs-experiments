let tasks = ['a','b','c'];
let response = {};

function processList(counter,finalCallback){
    if(counter < tasks.length){
        console.log('start Processing:',tasks[counter])

        callAsyncFunction(tasks[counter],function(res){
             console.log('Processing done:',tasks[counter],res);
             response[tasks[counter]] = res;
             processList(counter+1,finalCallback);
        });
    }
    else{
        finalCallback();
    }
}

function callAsyncFunction(input,cb){
    console.log('now Processing:',input);
    setTimeout(function(){           
            cb(input+'z');
        },1000);
}


function lastMethod(){
    console.log("Exitting...",response);

}

processList(0,lastMethod);

