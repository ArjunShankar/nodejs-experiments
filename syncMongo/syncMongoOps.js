//demonstrative code for syncing mongo operations 

var dbConn = require('mongoose');
dbConn.connect('mongodb://localhost/testDB');

var db = dbConn.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log('Connected to mongodb');
});

var deviceSchema = dbConn.Schema(
    {
        model:String,
        type:String
    }
);

var DeviceModel = dbConn.model('devices',deviceSchema);

var dump = [
    {
    model: 'x96', type: 'mobile2'
    },
    {
    model: 'x97', type: 'mobile3'
    },
    {
    model: 'x98', type: 'mobile4'
    }
];

function readData(){

    DeviceModel.find({},function(err,res){
        if(err){
            console.log(err);
        }
        else if(res){
            console.log("READ DATA",res);
        }

    })
}

var processItems = function(x,callback){
   if( x < dump.length ) {
       console.log("will call write for::",x);
       var obj = new DeviceModel(dump[x]);
        obj.save(function(err,res){
        if(err){
            console.log("write error");
        }
        else if(res){
            console.log("write success",res);
            processItems(x+1,callback);
        }
     })
      
   }
   else{
       console.log("Reached the end.. will read now..")
       callback();
   }
};

processItems(0,readData);
//readData();



/*
setTimeout(function(){
    console.log("called after timeout");
},0);

console.log("YULLO");


function* generatorFn () {
  console.log('look ma I was suspended')
}
var generator = generatorFn() // [1]
generator.next()


*/
/*
var db = dbConn.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log('Connected to mongodb');
});

var deviceSchema = dbConn.Schema(
    {
        model:String,
        type:String
    }
);

var DeviceModel = dbConn.model('devices',deviceSchema);

var dump = [
    {
    model: 'x96', type: 'mobile2'
    },
    {
    model: 'x97', type: 'mobile3'
    },
    {
    model: 'x98', type: 'mobile4'
    }
];
var counter =0;
var isLast = false;
for (item of dump){
    ++counter;
    if(counter == dump.length){
        isLast = true;
    }
    (function(input,flag){
        var obj = new DeviceModel(input);
        obj.save(function(err,res){
        if(err){
            console.log("write error");
        }
        else if(res){
            console.log("write success",res);
            if(flag){
                readData();
            }
        }
     })
    })(item,isLast)
    
    
}

function readData(){

    DeviceModel.find({},function(err,res){
        if(err){
            console.log(err);
        }
        else if(res){
            console.log("READ DATA",res);
        }

    })
}

*/
